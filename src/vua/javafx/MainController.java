package vua.javafx;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.Set;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.FlowPane;

public class MainController implements Initializable {

    private static final String LETTERS = "ABCČĆDĐEFGHIJKLMNOPRSŠTUVZŽ";

    private String targetWord;
    private int moveCounter, missCounter;
    private Random rand;

    @FXML
    private ImageView imgHangman;
    @FXML
    private FlowPane flowLetters, flowTargetWord;
    @FXML
    private Label lblResult;

    @FXML
    private void onLetterPressed(KeyEvent event) {
        String input = event.getText().toUpperCase();
        if (!LETTERS.contains(input)) {
            return;
        }

        for (int i = 0; i < LETTERS.length(); i++) {
            char ch = LETTERS.charAt(i);

            if (Character.toString(ch).equals(input)) {
                Button targetBtn = (Button) flowLetters.getChildren().get(i);
                if (targetBtn.isDisabled()) {
                    return;
                }
                targetBtn.setDisable(true);

                handleInput(input);
                break;
            }
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        rand = new Random();
        initScene();
    }

    private void initScene() {
        imgHangman.setImage(new Image("file:resources/images/hangman-start.png"));
        initLetters();
        targetWord = pickRandomWord();
        prepareTargetWordFields(targetWord);
    }

    private void restartGame() {
        flowLetters.getChildren().clear();
        flowTargetWord.getChildren().clear();
        moveCounter = missCounter = 0;
        lblResult.setText("");
        initScene();
    }

    private String pickRandomWord() {
        List<String> words = new ArrayList<>();

        try (BufferedReader reader = new BufferedReader(new FileReader("resources/dictionary/words.txt"))) {
            String line;
            while ((line = reader.readLine()) != null) {
                words.add(line.trim().toUpperCase());
            }
        } catch (IOException e) {
            System.err.print(e.getMessage());
            if (words.isEmpty()) {
                words.add("ERROR");
            }
        }

        return words.get(rand.nextInt(words.size()));
    }

    private void prepareTargetWordFields(String word) {
        for (int i = 0; i < word.length(); i++) {
            TextField txtLetter = new TextField();
            txtLetter.setEditable(false);
            txtLetter.setPrefWidth(40);
            txtLetter.getStyleClass().add("txtLetter");

            flowTargetWord.getChildren().add(txtLetter);
        }
    }

    private void initLetters() {
        for (int i = 0; i < LETTERS.length(); i++) {
            char ch = LETTERS.charAt(i);

            Button letterBtn = new Button(Character.toString(ch));
            letterBtn.getStyleClass().add("btnLetter");
            letterBtn.setOnAction((ActionEvent event) -> {
                Button source = (Button) event.getSource();
                source.setDisable(true);
                handleInput(source.getText());
            });

            flowLetters.getChildren().add(letterBtn);
        }
    }

    private void handleInput(String input) {
        moveCounter++;
        if (targetWord.contains(input)) {
            showHit(input);
        } else {
            missCounter++;
            showMiss();
        }
    }

    private void showMiss() {
        lblResult.setText("MISS");
        lblResult.getStyleClass().clear();
        lblResult.getStyleClass().add("lblMiss");

        if (missCounter >= 1 && missCounter <= 5) {
            String imgPath = String.format("file:resources/images/hangman-miss%s.png", missCounter);
            imgHangman.setImage(new Image(imgPath));
        } else {
            imgHangman.setImage(new Image("file:resources/images/hangman-gameover.png"));
            writeCorrectWord();
            showGameOverDialog();
        }
    }

    private void showHit(String input) {
        lblResult.setText("HIT");
        lblResult.getStyleClass().clear();
        lblResult.getStyleClass().add("lblHit");

        for (int i = 0; i < targetWord.length(); i++) {
            char ch = targetWord.charAt(i);

            if (Character.toString(ch).equals(input)) {
                TextField targetTxt = (TextField) flowTargetWord.getChildren().get(i);
                targetTxt.setText(input);
            }
        }

        int correctMoves = moveCounter - missCounter;
        if (correctMoves == getDistinctLettersCount()) {
            showGameWinDialog();
        }
    }

    private int getDistinctLettersCount() {
        Set<Character> hashLetters = new HashSet<>();
        for (int i = 0; i < targetWord.length(); i++) {
            hashLetters.add(targetWord.charAt(i));
        }
        return hashLetters.size();
    }

    private void writeCorrectWord() {
        for (int i = 0; i < targetWord.length(); i++) {
            TextField targetTxt = (TextField) flowTargetWord.getChildren().get(i);
            targetTxt.setText(Character.toString(targetWord.charAt(i)));
        }
    }

    private void showGameOverDialog() {
        showFinishDialog("Game Over", "You lose the game!");
    }

    private void showGameWinDialog() {
        showFinishDialog("Game Win", "You win the game!");
    }

    private void showFinishDialog(String title, String header) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText("Do you want to restart the game?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            restartGame();
        } else {
            Platform.exit();
        }
    }
}
